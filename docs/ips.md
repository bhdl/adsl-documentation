# Indoor Positioning System - IPS

Az **RTLS** REST API felületét pollingozva a lokális hálózaton jut hozzá
a **tag**-ek **anchor**-okhoz mért távolságához.

A szolgáltatás kiszámítja a **tag**-ek
relatív 3 dimenziós koordinátáit, amelyet **Websocket (SocketIO)** kapcsolaton keresztül
a lokális hálózaton továbbít a **Gateway** alkalmazásnak.

## Rendszer Felépítése

![IPS Overview](img/ips_overview.bmp)

## Szolgáltatás Logikai Felépítése

![IPS System Logical Flow](img/ips_flowchart.bmp)

# Websocket Kommunikáció

## Végpont

`positionSocketAddress` paraméter tartalmazza, pl. `/gateway/${gatewayId}/ips`

## Események

### emit - message

IPS adatokat küldi a gatewaynek.

**Példa**

```json
{
  "timestamp": 1534313277452,
  "clientId": "ips01",
  "type": "postion",
  "packageId": "ips01_2018_08_21_10_43_21_123",
  "data": {
    "positions": [
      {
        "tagId": "1",
        "timestamp": 1534313267452,
        "relativeLocation": {
          "x": 1.6,
          "y": 12.45,
          "z": 2.4,
          "accuracy": 0.12
        },
        "geolocation": {
          "latitude": 1.23455,
          "longitude": 1.23344,
        }
      }
    ]
  }
}
```

#### Adatcsomag leírás

| Név                                           | Típus  | Leírás                                                   |
|-----------------------------------------------|--------|----------------------------------------------------------|
| timestamp                                     | Long   | adatküldés időpontja UNIX-időben                         |
| clientId                                      | String | kliens On Site System szinten egyedi azonosítója         |
| type                                          | String | küldött információ osztályozása                          |
| packageId                                     | String | küldött csomag azonosítója: client_id + "_" + timestamp  |
| data                                          | Object | adatcsomag                                               |
| data.positions                                | List   | mért pozíciós adatok listája                             |
| data.positions[i]                             | Object | mért pozíciós adatok                                     |
| data.positions[i].tagId                       | String | RTLS - tag azonosítója                                   |
| data.positions[i].timestamp                   | Long   | mérés időpontja UNIX-időben                              |
| data.positions[i].relativeLocation            | Object | relatív koordinátához köthető pozíciós adatok            |
| data.positions[i].relativeLocation.x          | Float  | mért pozíció relatív x koordinátája méterben             |
| data.positions[i].relativeLocation.y          | Float  | mért pozíció relatív y koordinátája méterben             |
| data.positions[i].relativeLocation.z          | Float  | mért pozíció z koordinátája méterben (magasság)          |
| data.positions[i].relativeLocation.accuracy   | Float  | mért pozíció pontossága méterben                         |
| data.positions[i].geolocation                 | Object | geokoordinátához köthető pozíciós adatok                 |
| data.positions[i].geolocation.latitude        | Float  | mért pozíció geolokációban mért pozíciója - szélesség    |
| data.positions[i].geolocation.longitude       | Float  | mért pozíció geolokációban mért pozíciója - hosszúság    |

# IPS beacon pozícionáló

## Buildelés

Java 8 ben készült a program. Min JRE 8-ra van szükség a futtatáshoz, és JDK 8-ra a compile-hoz.

//TODO: build command maven-hez

Használt libraryk:
- springframework boot - 2.0.3.RELEASE
- gson - 2.8.5
- json - 20180813
- mongoDb java driver - 3.8.1
- junit - 3.8.1

## Szerver követelmények

- java futtatókörnyezet

## Futtatás

A telepítéshez másoljuk be az alábbi fileokat egy mappába:
- positioning.jar
- settings.properties
- detectors.properties
- collisionDetectionIDs.properties

Indítás: ```java -jar positioning.jar```

Ha a program nem találja valamelyik filet, indulásnál hibaüzenettel leáll.

# Konfigurálási lehetőségek

A program a jar melletti .properties fileokból olvassa be a futási paramétereket, detektorokat és az ütközés detektálandó IPS ID-kat. Ezen kívül létrehoz 2 .dat filet a socketen sikertelenül küldött adatok buffereléséhez.

## detectors.properties file
Ez a file az RTLS detektorok adatait tartalmazza json arrayban.
Minimum 4 detektornak kell látnia egyszerre egy beacont, ahhoz, hogy pozícionálni lehessen.
A detektorok helyzete relatív koordinátákban értendő az origótól, descartes koordináta rendszerben (x,y,z tengelyen).
A detektorok ID-ja egy állandó string.
Az RTLS szerver API válasza tartalmazza: http://RTLS_IP:RTLS_PORT/changes/

Példa file tartalom:
```
- detectors=[{"ID":"1","x":1.256,"y":4.95,"z":2.106},{"ID":"2","x":4.781,"y":3.588,"z":0.671},{"ID":"3","x":3.269,"y":4.735,"z":1.574},{"ID":"4","x":0.444,"y":1.551,"z":1.403},{"ID":"5","x":1.464,"y":0.265,"z":1.6}]
```

## collisionDetectionIDs.properties file
Egy json array-t tartalmaz, ami az ütközés detekcióhoz használt IPS beaconok ID-jait tárolja.

Példa tartalom:
```
- checkableIDs = ["16","32"]
```

## settings.properties

Ez a file a program változóit tartalmazza.

Ezek a program indulásakor olvasódnak be.

Ha módosítani szeretnénk őket, újra kell indítani a programot.

- `clientID` A pozíciókhoz ezt a clientId-t küldi az alkalmazás.
- `positionSocketAddress` A Gateway APP IPS socket szerver url-je. Ezen mennek fel a pozíció adatok.
- `collisionDetectionSocketAddress` A Gateway APP ütközés detekciós socket szerver urlje.
- `positionSocketTag` A pozíció felküldő socket emit eseménye.
- `collisionSocketTag` Az ütközés detekciós socket emit eseménye.
- `socketTimeout` Minden socket kapcsolat timeoutja.
- `apiURL` Az RTLS api-ja, amit pollingol a rendszer.
- `apiTimeout` Az RTLS-t pollingoló API timeoutja.
- `pollingTime` Az RTLS api polling időköze.
- `signalsBufferIntervall` Az RTLS től érkezett beacon távolságok egy bufferbe kerülnek és ennyi mp-ig tárolódik.

    A pozíció számításnál átlagolásra kerülnek ezek az értékek.

    Az átlagolás segítségével, ha egy api csak néha néha látna egy beacont, akkor is lesz róla detektortól számított távolság adat.

- `origoOfTheRelativeCoordinateSystem`: az épület legnyugatibb, legdélebbi és legmélyebb pontja. GPS coordinátákban értendő.

    Ehhez a ponthoz képest lesznek hozzáadva a pozíciók, távolság és irány szerint.

    A kiszámított, relatív koordinátás pozíciók átváltásához is szükséges.

    **Relatív Koordinátarendszer, Koordináta Rendszer Felépítése**

    Az **X**, **Y** és **Z** értékek az origótól való távolságot jelentik méterben.

![Relative Coordinate System Setup](img/relative_pos_coords.bmp)


**Koordináta Rendszer Viszonyulása a Geolokációs Adatokhoz**

![Relative Coordinate System and Geolocation](img/relative_pos_origo.bmp)

- `relativeCoordinateSystemAngleComparedToEarth`: Az épület (relatív koordináta rendszer) dőlésszöge fokban északi irányhoz képest. +90 fok, ha az épület keletnek néz. Tartomány: 0-360.
- `collisionDetectionDistanceThreshold`: Az ütözés detekciónál ezen a távolságon belül kell lennie 2 ütközés detekcióra figyelt IPS beaconnak, méterben.

### settings.properties alapértékei

```
clientID = ips01
positionSocketAddress = http://GATEWAY_IP:GATEWAY_APP_PORT/ips
collisionDetectionSocketAddress = http://GATEWAY_IP:GATEWAY_APP_PORT/collision-avoidance
positionSocketTag = message
collisionSocketTag = ca-emergency
socketTimeout = 5000
apiURL = http://RTLS_IP:RTLS_PORT/changes/
apiTimeout = 7500
pollingTime = 250
signalsBufferIntervall = 1000
origoOfTheRelativeCoordinateSystem={"latitude":47.680979,"longitude":19.265199,"altitude":167.0}
relativeCoordinateSystemAngleComparedToEarth = 47.94
collisionDetectionDistanceThreshold = 0.25
```

## Bufferek

Ha megszakad a kapcsolat, vagy error választ ad a socket az aktuális adatot azonnal ebbe a fileba menti a program.
Program indulásnál betölti a tartalmukat és megpróbálja újra elküldeni.
Mindig a legrégebbi bufferben lévő adatot próbálja elküldeni a program.
Másodpercenként folyamatosan próbálkozik a küldéssel megszakadt kapcsolat esetén.

- socketCollisionBuffer.dat: Ebbe menti a megszakadt kapcsolatnál elküldött ütközés detekció adatokat.
- socketPositionBuffer.dat: Ebbe menti a sikertelenül felküldött beacon pozíció adatokat.


## Működési elv

Kiindulási alapok:

- Van legalább 4 db detektorunk ismert helyzettel, amik távolságot mérnek a beacon-től.
- A jelek terjedési sebessége jó közelítéssel fénysebesség. (Ez teljesül mindig)

### IPS beaconok távolság számítása

A jeladó kibocsát egy timestampet, amit a detektorok rögzítenek és megmérik mennyi idő alatt jutott el hozzájuk a jel. Ebből pedig minden detektor kiszámítja a jeladó távolságát magához képest.

Legalább 4 detektor szükséges. A detektorok egy-egy jeladó távolság sugarú gömböt jelölnek ki a térben, amelyek felületén valamely pontban helyezkedhet el a jeladó.

A jeladó helye ideális esetben ott lesz, ahol mindegyik gömb metszi egymást. A valóságban szinte sosem lesz metszet. A mért távolság pontossága a rádióhullámok interferenciája, törése és közegenként változó terjedési sebessége miatt mindig eltér egy kicsit.

Fontos: Az előbbi tényezők csakis hozzáadáódhatnak a mérési időhöz, ezzel a távolsághoz!

### Pozícionálás
A jeladó helyzete az a pont lesz, ami a legközelebb van mindegyik gömb felületéhez, de lehetőleg a gömbökön belül van!

### Számítás módja
Minden detektorra elvégezzük az alábbi számítást.

Vesszük a vizsgált detektor pozíciójának távolságát a gömb felülettől, megnézzük a különbséget, négyzetre emeljük, majd szorozzuk az adott detektor accuracy-jával (ez egy tetszőleges szám, a mérés súlya).

Ha a vizsgált pozíció távolabb van, mint a vizsgált gömb sugár, megszorozzuk az értéket egy penalty factorral.

Azért nem zárjuk ki egyből a helyzetet, mert rosszul hangolt detektorok esetén elképzelhető, hogy tényleg a gömbön kívüli a pozíció, és kell egy folytonos függvény a tényleges pozíció konvergencia számításhoz.

Mindegyik detektorra elvégezzük ezt a számítást és összeadjuk az összegeket.

Azt a pozíciót keressük, ahol az az összeg minimális.

A minimum keresés megvalósításához, deriváljuk a függvényt, (megadja a meredekségét az adott értéknél), majd megnézzük, hol lesz 0 a derivált. Ott lesz egy lokális minimum és egy valószínű pozíció.

Sajnos a derivált függvény lokális minimumait nem lehet analitikusan, matematikailag kiszámítani. Több x,y,z (pozíció) megoldás is lehetséges. Ezért egy momentum optimalizált stochasztikus gradient descent (SGD) módszerrel keressük meg a lokális minimumokat és velük a pozíciót.

### SGD
A detektorok helyzetétől indul ki. Kiszámítja az adott ponton a deriváltat. Ez ad egy irányt és nagyságot a függvény változásáról (gradienst).

Majd a nagyság (abszolut érték) töredékével lépünk abba az irányba, és megnézzük ott is a gradiens vektort.

Ezt iteratívan ismételjük, amíg a gradiens be nem konvergál egy lokális minimumba.

Minden detektortól elindítok egy SGD konvergenciát és megnézzük milyen lokális minimumba vándorolnak bele.

A legkissebb függvény értékkel (összegzett négyzetes detektor távolság különbséggel) rendelkező lokális minimum pozíciója lesz a legvalószínűbb tartózkodási érték.

A lokális minimum nagyságából már következtethetünk a mérési  pontosságra is.

(Analógia: Olyan, mint ha egy ködös hegyről mennénk le a völgybe. Megnézzük merre a leg lejtősebb a hely, pár lépést arra megyünk, majd így tovább. Ha sokáig tart egy irányba nagyobbakat lépünk.Végül lejutunk a völgybe. Több völgy (lokális minimum) is lehet, a legményebb kell nekünk.)

