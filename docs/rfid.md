# RFID Service

Az **RFID Service** egy a drónon lévő **Raspberry PI** eszközön fut. Soros porton keresztül olvassa
ki az RFID információkat az RFID olvasó és 3 db RFID antenna hardver komponensekből. A szolgáltatás
adott időpontban kiolvasott RFID azonosítókat az észlelt jelerőséggel továbbítja a **Gateway**
alkalmazásnak **Websocket (SocketIO)** kapcsolaton keresztül a lokális hálózaton.

# Socket Kommunikáció Leírása

## Végpont

`/gateway/${gatewayId}/rfid`

## Események

### emit - message

Gateway App-nak Küldött Adatcsomag

**Példa**

```json
{
  "timestamp": 1534313277452,
  "clientId": "cl01",
  "type": "rfid",
  "packageId": "cl01_2018_08_21_10_43_21_123",
  "data": {
    "readerStatus": {
      "temperature": 48
    },
    "rfData": [
      {
        "tagId": "rf05",
        "antennaId": "an02",
        "rssi": 0,
        "timestamp": 1534313427452
      }
    ]
  }
}
```

#### Adatcsomag Leírás

| Név                           | Típus   | Leírás                                                         |
|-------------------------------|---------|----------------------------------------------------------------|
| timestamp                     | Long    | adatküldés időpontja UNIX-időben                               |
| clientId                      | String  | kliens On Site System szinten egyedi azonosítója               |
| type                          | String  | küldött információ osztályozása                                |
| packageId                     | String  | küldött csomag azonosítója: client_id + "_" + timestamp        |
| data                          | Object  | adatcsomag                                                     |
| data.readerStatus             | Object  | Rf reader szükséges adatai                                     |
| data.readerStatus.temperature | Integer | Rf reader hőmérséklete                                         |
| data.rfData                   | List    | észlelt RFID-k listája                                         |
| data.rfData[i].tagId          | String  | észlelt RFID egyedi azonosítója az adott telephelyen           |
| data.rfData[i].antennaId      | String  | antenna azonosítója, amely észlelte az RFID-t                  |
| data.rfData[i].rssi           | Integer | adáserősséget jellemző adat. (0 - ~ -100dB)                    |
| data.rfData[i].timestamp      | Long    | észlelés időpontja UNIX időben                                 |
