# Socket Kommunikáció Általános Felépítése

## Szervernek Küldött Adatcsomag Általános Szerkezete

```json
{
  "timestamp": 1534313277452,
  "clientId": "dr01",
  "type": "postion",
  "packageId": "dr01_2018_08_21_10_43_21_123",
  "data": {}
}
```

| Név       | Típus  | Leírás                                                                           |
|-----------|--------|----------------------------------------------------------------------------------|
| timestamp | Long   | adatküldés időpontja UNIX-időben                                                 |
| clientId  | String | kliens On Site System szinten egyedi azonosítója                                 |
| type      | String | küldött információ osztályozása ("postion", "rfid", etc.)                        |
| packageId | String | küldött csomag azonosítója: client_id + "_" + timestamp yyyy_MM_dd_hh_mm_ss_SSS  |
| data      | Object | adatcsomag                                                                       |

## Szervertől Érkező Csomagok Általános Felépítése

| Név       | Típus   | Leírás                                                                          |
|-----------|---------|---------------------------------------------------------------------------------|
| status    | String  | feldolgozás sikerességét osztályozó karakterlánc                                |
| code      | Integer | feldolgozás sikerességét osztályozó HTTP RESPONSE kód                           |
| message   | String  | felhasználó által értelmezhető üzenet                                           |
| packageId | String  | küldött csomag azonosítója: client_id + "_" + timestamp yyyy_MM_dd_hh_mm_ss_SSS |
| data      | Object  | válaszadatok                                                                    |

## Szervertől Érkező Csomagok Általános Felépítése Sikeres Feldolgozás Esetén

```json
{
  "status": "OK",
  "code": 200,
  "message": "Something useful",
  "packageId": "dr01_2018_08_21_10_43_21_123",
  "data": {}
}
```

## Szervertől Érkező Csomagok Általános Felépítése Sikertelen Feldolgozás Esetén

```json
{
  "status": "ERROR",
  "code": 404,
  "message": "Something happened",
  "packageId": "dr01_2018_08_21_10_43_21_123",
  "data": {}
}
```
