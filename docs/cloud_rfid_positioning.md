# Cloud RFID pozícionáló rendszer, Data Mapper

## Leírás

A Data Mapper alkalmazás az RFID tagek pozícióját számítja ki az IPS és RFID mérések kombinálásával. Megnézi hol volt az adott RFID detektor a mérés pillanatában és milyen jelerőséggel mérte a taget.
A program egy API felületet nyújt, amin keresztül elindítható a leltározási folyamat.
A feldolgozás végén a Data Mapper elküldi az adatokat a Cloud Webappnak a POST /v1/inventoryStatus végpontra, egyszerre csak maxInventoryRfidPositionsInApiCall darabot. A választ megvárva küldi a következő csomagot. Az API a Webappnál dokumentálva van.

## Buildelés

Java 8 ben készült a program. Min JRE 8-ra van szükség a futtatáshoz, és JDK 8-ra a compile-hoz.

//TODO: build command maven-hez

Használt libraryk:
- springframework boot - 2.0.3.RELEASE
- gson - 2.8.5
- json - 20180813
- mongoDb java driver - 3.8.1
- junit - 3.8.1

## Szerver követelmények

- java futtatókörnyezet

## Futtatás

Az alábbi fileokat, mappákat kell feltenni egy szerver mappába:
- rfid_positioning.jar
- settings.properties
- rfid_positioning_lib

Indítás a szervermappán belül:
```java -jar rfid_positioning.jar```

Ha a program nem találja a setting.properties filet, vagy a libraryket tartalmazó mappát indulásnál, hibaüzenettel leáll:

- ERROR: Cannot parse detectors from file!
- ERROR: Cant open properties file, creating one instead!

## Service

Service létrehozása daemonként.

## Konfigurálási lehetőségek

A program a jar melletti settings.properties fileból olvassa be a futási paramétereket.

- apiPort: Az alkalmazás által biztosított API portja, amire a cloud webapp küld eseményt: http://[CLOUD_IP]:[apiPort]/rest/v1/startInventoryPositioning
- mongoDbUrl: A mongoDb adatbázis elérési URL-je.
- mongoDbPort: A mongoDb portja.
- adslDatabaseName: A mongoDb-s adatbázis neve.
- positioningCollectionName: Az IPS méréseket tartalmazó collection neve. (Kissé más formátumban tárolódnak az adatok, mint ahogy a socketen át érkeznek.)
- rfidCollectionName: Az RFID méréseket tartalmazó collection neve. (Ez is kissé más formátumban tárolódik le.)
- inventoryResultApiUrl: A Webapp inventoryStatus API URL-je. A kiszámított RFID tag pozíciókat ide tölti fel a program.
- apiTimeout: Az inventoryStatus API timeoutja, max ennyi mp-ig vár a válaszra.
- maxInventoryRfidPositionsInApiCall: Az inventoryStatus API-ra maximum ennyi RFID pozíciót fog csak egyszerre felküldeni a program a Webapp számára. Ha több van, egymás után több hívást indít.


Setting.properties alapértékei:
- apiPort = [CLOUD_DATAMAPPER_PORT]
- mongoDbUrl = [CLOUD_IP]
- mongoDbPort = [CLOUD_MONGO_PORT]
- adslDatabaseName = adsl
- positioningCollectionName = ips
- rfidCollectionName = rfid
- inventoryResultApiUrl = http://[CLOUD_HOST]/rest/v1/inventoryStatus
- apiTimeout = 5000
- maxInventoryRfidPositionsInApiCall = 100

## REST API

### Az alkalmazás értesítése a leltározási esemény végéről

POST Kérés: ```/rest/v1/startInventoryPositioning```

Kérés példa

```json
{
    data: {
        startTime: 1538460021,
        endTime: 1538460035,
        gatewayId: "gateway-id",
        inventoryEventId: 15,
        siteId: 1,
        tag: [
            {
                clientId: "1",
                description: "Indoor",
                name: "48-as tag",
                siteId: 1,
                tagId: 1,
                tagToken: "48",
                type: "forklift"
            }
        ]
    }
}
```

#### Kérés elemei

- startTime: leltározási esemény kezdőpontja unix timestamp, másodpercben
- endTime: leltározási esemény végpontja unix timestamp, másodpercben
- gatewayId: gateway azonosító
- inventoryEventId: leltárzoási esemény id
- siteId: telephely id
- tag: tag-ek objektum tömbje
    - clientId: kliens azonosító
    - description: Tag leírása
    - name: Tag neve
    - siteId: telephely azonosító
    - tagId: Tag id
    - tagToken: Tag token, felprogramozott azonosító
    - type: tag típusa, string

### Válasz példa

```json
{
    code: "success",
    data: {
        gatewayId: "gateway-1",
        siteId: 1
    },
    message: "Inventory started!",
    status: 200
}
```

### Válasz elemei

- code: "success" vagy "error"
- message: a válasz szöveges magyarázata
- status: 200 vagy 500
- data:
    - gatewayId: Gateway azonosító
    - siteId: Telephely azonosító

## Működési elv:

Az előzőkhöz hasonlóan ismerjük a drónon lévő RFID reader helyzetét a mérés pillanatában, valamint látjuk a tagektől kapott jelerősséget decibel-ben (RSSI).

Ebből elvileg lehetne távolságot számítani (minél kisebb a jelerősség annál távolabb van), az IPS-es módszer alapján de a mérések annyira megbízhatatlanok, hogy a távolság számítás pontossága sokszor nagyobb mint a becsült érték. Ezért sajnos értelmetlen lenne az IPS-es módszert alkalmazni.

Minden RFID mérési pozíciónál kiszámítja az alkalmazás, hogy a milyen távol lehet tőle a drón, és a mérések helyét ezzel súlyozva átlagolja az RFID tag pozíciójának számításához.

A jövőben a drón orientációjának figyelembe vételével esetleg tovább pontosítható a pozícionálás.
