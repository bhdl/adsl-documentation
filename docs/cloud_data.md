# Cloud Data Server Leírása

A Cloud Data feladat a telephelyeken lévő gateway-eken keresztül
érkező adatok fogadása, ellenőrzése, transzformációja illetve
eltárolása.

A Cloud Data az IPS adatokat real time továbbítja a frontend kliens
könyvátárának.

A Cloud Data REST API felületen keresztül ad hozzáférést
a tárolt adatohoz a rendszer további micro service-inek

# Tárolt Dokumentumok

A Cloud Data rendszerben tárolt adatok IPS és RFID adatok lehetnek. A következőkben részletezzük ezek felépítését.

## IPS Adat

A Gatewaytől érkező IPS adatcsomag.

### Példa Dokumentum

```json
{
  "packageId": "gateway01_2018_08_21_10_43_21_123",
  "gatewayId": "gateway01",
  "clientId": "gateway01",
  "tagId": "tag01",
  "timestamp": 1535972524226,
  "relativeLocation": {
    "x": 126.39,
    "y": 272.02,
    "z": 12,
    "accuracy": 0.36
  },
  "geolocation": {
    "latitude": 12.344995,
    "longitude": 43.238344
  }
}
```

### Adatok Leírása

| Név                                           | Típus  | Leírás                                                   |
|-----------------------------------------------|--------|----------------------------------------------------------|
| packageId                                     | String | küldött csomag azonosítója: client_id + "_" + timestamp  |
| gatewayId                                     | String | gateway - telephely azonosítója                          |
| clientId                                      | String | kliens On Site System szinten egyedi azonosítója         |
| tagId                                         | String | RTLS - tag On Site egyedi azonosítója                    |
| timestamp                                     | Long   | mérés időpontja UNIX-időben                              |
| relativeLocation                              | Object | relatív koordinátához köthető pozíciós adatok            |
| relativeLocation.x                            | Float  | mért pozíció relatív x koordinátája méterben             |
| relativeLocation.y                            | Float  | mért pozíció relatív y koordinátája méterben             |
| relativeLocation.z                            | Float  | mért pozíció z koordinátája méterben (magasság)          |
| relativeLocation.accuracy                     | Float  | mért pozíció pontossága méterben                         |
| geolocation                                   | Object | geokoordinátához köthető pozíciós adatok                 |
| geolocation.latitude                          | Float  | mért pozíció geolokációban mért pozíciója - szélesség    |
| geolocation.longitude                         | Float  | mért pozíció geolokációban mért pozíciója - hosszúság    |

## RFID Adat

A Gatewaytől érkező RFID adatcsomag.

### Példa Dokumentum

```json
{
  "packageId": "gateway01_2018_08_21_10_43_21_123",
  "gatewayId": "gateway01",
  "clientId": "gateway01",
  "readerTemperature": 32.6,
  "timestamp": 1535972524226,
  "tagId": "tag01",
  "antennaId": "antenna01",
  "rssi": 54.2
}
```

### Adatok Leírása

| Név                                           | Típus   | Leírás                                                   |
|-----------------------------------------------|---------|----------------------------------------------------------|
| packageId                                     | String  | küldött csomag azonosítója: client_id + "_" + timestamp  |
| gatewayId                                     | String  | gateway - telephely azonosítója                          |
| clientId                                      | String  | kliens On Site System szinten egyedi azonosítója (drón)  |
| readerTemperature                             | Float   | olvasó hőmérséklete Celsiusban                           |
| timestamp                                     | Long    | mérés időpontja UNIX-időben                              |
| tagId                                         | String  | észlelt RFID egyedi azonosítója az adott telephelyen     |
| antennaId                                     | String  | antenna azonosítója a hordozón                           |
| rssi                                          | Integer | adáserősséget jellemző adat. (0 - ~ -100dB)              |


# IPS Socket Kommunikáció

A gateway ezen a végponton is csatlakozik a Cloud Data Serverhez, amin csak az IPS adatokat küldi.

## Végpont

```javascript
`/gateway/${gatewayId}/ips`
```

## Események

### on - message

IPS adatokat fogadja az adott gateway felől. Az elvárt adatszerkezet megegyezik
az [On Site System IPS-nél taglaltakkal](./ips.md), a `gatewayId`-val kiegészítve.

### emit - response

A `message` eseményt emitáló socket kliensnek adott válasz. A playload
data része még nem definiált.

*Sikeres Válasz*

```javascript
{
  "status": 200,
  "code": "OK",
  "message": "The sended data is proccessed",
  "packageId": "gateway01_2018_08_21_10_43_21_123",
  "data": {}
}
```

*Invalid adat válasz*

```json
{
  "status": 400,
  "code": "ERROR",
  "message": "The sended data is invalid",
  "packageId": "gateway01_2018_08_21_10_43_21_123",
  "data": {}
}
```

*Adatok eltárolása sikertelen válasz*

```json
{
  "status": 503,
  "code": "ERROR",
  "message": "The sended data was not saved",
  "packageId": "gateway01_2018_08_21_10_43_21_123",
  "data": {}
}
```

### emit - mapdata

Lásd [Cloud Map](./cloud_map.md)

# RFID Socket Kommunikáció

A gateway ezen a végponton is csatlakozik a Cloud Data Serverhez, amin csak az RFID adatokat küldi.

## Végpont

```javascript
`/gateway/${gatewayId}/rfid`
```

## Események

### on - message

RFID adatokat fogadja az adott gateway felől. Az elvárt adatszerkezet megegyezik
az [On Site System RFID-nél taglaltakkal](./rfid.md), a `gatewayId`-val kiegészítve.

### emit - response

A `message` eseményt emitáló socket kliensnek adott válasz. A playload
data része még nem definiált.

*Sikeres Válasz*

```json
{
  "status": 200,
  "code": "OK",
  "message": "The sended data is proccessed",
  "packageId": "gateway01_2018_08_21_10_43_21_123",
  "data": {}
}
```

*Invalid adat válasz*

```json
{
  "status": 400,
  "code": "ERROR",
  "message": "The sended data is invalid",
  "packageId": "gateway01_2018_08_21_10_43_21_123",
  "data": {}
}
```

*Adatok eltárolása sikertelen válasz*

```json
{
  "status": 503,
  "code": "ERROR",
  "message": "The sended data was not saved",
  "packageId": "gateway01_2018_08_21_10_43_21_123",
  "data": {}
}
```

# REST API Felület

A Cloud Data REST API felületet nyújt a Cloud Data Map szolgáltatás számára.

Amikor a Data Map elkészült az adatok feldolgozásával, erre a végpontra küldi vissza az adatokat.

## Végpont

```javascript
GET: `/api/${version}/gateways/${gatewayId}/route`
```

### Paraméterek

- `start` (timestamp): 1537177264
- `end` (timestamp): 1537187264
- `tagId` (string): tag123

Az intervallum legfeljebb 3 óra lehet.

*Sikeres válasz*

```json
{
  "status": 200,
  "code": "SUCCESS",
  "message": "Get positions in interval",
  "data": {
    "positions": [
      {
        "latitude": 0,
        "longitude": 0
      },
      {
        "latitude": 1,
        "longitude": 1
      }
    ]
  }
}
```

*Hiba esetén*

```json
{
  "status": 404,
  "code": "ERROR",
  "message": "Resource not found"
}
```

```json
{
  "status": 400,
  "code": "ERROR",
  "message": "The interval cannot exceed 3 hours"
}
```

# Cloud Data Implementáció

## Socket.io Controller

Az alábbi UML Diagram bemutatja, hogy az alkalmazás egyes komponensei hogyan épülnek fel.

- `IO` - a Socket.IO modul
- `BaseController` - Ősztály, amellyel inicializálni lehet egy route-on middleware-k és eventhandler-ek listáját.
- `StaticNameSpaceController` - statikus string route alkalmazására
- `DynamicNameSapceController` - regexp route alkalmazására, az IO modul mintaillesztésével kapcsolódnak a kliensek
- `EventHandler` - event megvalósításának ősosztálya
- `EventHandlerMethod` - egy konkrét event megvalósítása

![Socket.io Controller UML](./img/socket.io.controller.uml.bmp)
