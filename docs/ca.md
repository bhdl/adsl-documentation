# Collision Avoidance System - CA

**Ütközés elhárító rendszer - IPS**

Az IPS Service-nél van eltárolva, hogy az egyes tag-ek milyen távolságban kell lenniük egymástól.
A rendelkezésre álló tag helyzetinformációk alapján, ha 2 tag relációjában megsérti a beállított
távolságot, akkor riasztást ad le a gateway irányában.

**Kliens**

A kliens saját oldalán tudja menedzselni, hogy mely tag-re iratkozik fel, egyszerűen
azzal, hogy csak adott eseményre figyel a megadott namespace-en.

A kliens feladata, hogy a kapott információkat feldolgozza, és jelezze, ha az adott taghoz közel került egy másik.

![Collision Avoidance System Overview](./img/adsl_collision_avoidance.bmp)

# Socket Kommunikáció Leírása

## Végpont

`/collision-avoidance`

## Események

### emit - `ca-emergency`

Az IPS csak egy eseményt emittál, újat csak akkor ad le, hogy ha probléma megszűnt és újra előfordult.

**Adatcsomag - példa**

```json
{
  "timestamp": 1534313277452,
  "clientId": "ips01",
  "type": "collision-avoidance",
  "packageId": "ips01_2018_08_21_10_43_21_123",
  "data": {
    "emergencies": [
      {
        "distance": 2.48,
        "accuracy": 0.75,
        "sharers": ["tag001", "tag002"]
      },
      {
        "distance": 2.48,
        "accuracy": 0.75,
        "sharers": ["tag001", "tag003"]
      }
    ]
  }
}
```

**Adatcsomag - leírás**

| Név                            | Típus  | Leírás                                                  |
|--------------------------------|--------|---------------------------------------------------------|
| timestamp                      | Long   | adatküldés időpontja UNIX-időben                        |
| clientId                       | String | kliens cloud szinten egyedi azonosítója                 |
| type                           | String | küldött információ osztályozása 'collision-avoidance'   |
| packageId                      | String | küldött csomag azonosítója: client_id + "_" + timestamp |
| data                           | Object | adatcsomag                                              |
| data.emergencies               | List   | figyelmeztetések listája                                |
| data.emergencies[i]            | Object | figyelmeztetés adatobjektum                             |
| data.emergencies[i].distance   | Float  | mért távolság                                           |
| data.emergencies[i].accuracy   | FLoat  | mért távolság pontossága                                |
| data.emergencies[i].sharers    | List   | figyelmeztetés érintettjei                              |
| data.emergencies[i].sharers[0] | String | figyelmeztetés egyik érintettje                         |
| data.emergencies[i].sharers[0] | String | figyelmeztetés másik érintettje                         |


### on - `ca-emergency-${tag-id}`

A kliens saját oldalán tudja menedzselni, hogy mely tag-re iratkozik fel, egyszerűen
azzal, hogy csak adott eseményre figyel a megadott namespace-en.

A kliens addig figyelmeztet a beérkező esemény hatására, ameddig a végfelhasználó
nem nyugtázza.

**Adatcsomag - példa**

```json
{
  "timestamp": 1534313277452,
  "clientId": "ips01",
  "type": "collision-avoidance",
  "packageId": "ips01_2018_08_21_10_43_21_123",
  "data": {
    "emergency": {
      "distance": 2.48,
      "accuracy": 0.75,
      "sharer": "tag002"
    }
  }
}
```

## Gateway

A gateway a következő végponton `/collision-avoidance` figyel
a következő eseményre `ca-emergency`. A Gateway feladata, hogy

- transzformálja az IPS-től érkező adatcsomagokat
- valamint emittálja a namespace-en kliensek számára a riasztási eseményeket.
