# REST API Kommunikáció Általános Felépítése.

# REST API Kérések Általános szerkezetet

## Header Sorok

* **content-type**: minden végpont esetében kötelező megadni az `application/json` értéket

## Body

* Minden egyes végpontnál más `JSON` végpontoként definiált szerkezet.

## Végpontok

A végpontokat verziózni kell, hogy később könnyebb legyen továbbfejleszteni a rendszert.

### Szerkezet

`/rest/${version-id}/${resource}`

### Példa

`/rest/v1/genre`

# REST API Válaszok Általános szerkezetet

## HTTP Státusz Kódok

* **200** - Általános minden rendben
* **201** - Erőforrás sikeresen létrehozva
* **400** - Rossz kérés, pl. invalid szintaxis.
* **404** - Invalid végpont. Az adott végpont nem létezik, az adott erőforrás nem létezik
* **410** - Az adott erőforrás nem létezik, mert törölték
* **415** - Kérés `content-type`-ja nem megfelelő (`application/json`)
* **500** - Szerver oldali hiba

## Header Sorok

* **content-type**: minden végpont esetében kötelező megadni az `application/json` értéket

## Sikeres válasz esetén Body általános szerkezete

* Minden egyes végpontnál más `JSON` végpontoként definiált szerkezet.

## Hibaválasz általános szerkezete példa

**Példa HTTP STATUS CODE**: 500

**Példa Body**:

```json
{
  "error": {
    "code": "ERR_INTERNA",
    "title": "Internal Server Error",
    "message": "Something happened!"
  }
}
```

# Dummy Resource

Ez egy olyan példa **erőforrás**, amely szemlélteti a REST API
végpontokkal kapcsolatos elvárásokat. A példa erőforrás egy műfaj (genre)
tag.

```json
{
  "id": 1,
  "name": "Rock&Roll"
}
```

# Adott Resource Lekérése

## HTTP Kérés

### Végpont

GET `/genre/${genre-id}` vagy GET `/genre/${genre-slug}`

A lényeg, hogy az adott erőforrást egyértelműen beazonosító jellemzővel
érhető el az adott erőforrás, mint pl. az adott rekord azonosítója,
vagy egy SLUG `/genre/rock-and-roll`. A listázó válaszban szerepelnie kell
ezen jellemzőnek!

## Sikeres Válasz

```json
{
  "data": {
    "id": 1,
    "name": "Rock&Roll"
  }
}
```

## Sikertelen válaszok

### Az adott erőforrást törölték

**HTTP CODE**: 410

```json
{
  "error": {
    "code": "ERR_RESOURCE_DELETED",
    "title": "Deleted Resource",
    "message": "The requested tag has been deleted"
  }
}
```

### Az adott erőforrás nem létezik

**HTTP CODE**: 404

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested tag does not exist"
  }
}
```

# Adott Resource Listázása

## HTTP Kérés

### Végpont

GET `/genre`

### Query Paraméterek

* **page** (Integer): Hanyadik oldal
* **number** (Integer): Hány resource legyen a válaszban.

## Sikeres Válasz

Minden erőforrás a data `object`-en belül a resource nevének töbessszám nevével
jelzett tömben lévő erőforrás objektumokkal tér vissza.

Listázó válasz esetén a válasznak tartalmaznia kell `pagination` object-et,
amely tartalmazza a lapozáshoz szükséges információkat:

- count: a feltételeknek megfelelő összes elemszám, ha nincs akkor 0
- per_page: egy válaszban visszaadott elemszám
- current_page: adott oldal száma 1-től kezdve, ha nincs akkor 0
- total_pages: összes oldalszám, ha nincs akkor 0
- next_url: a következő oldal lekérdezéséhez szükséges url. Ha nincs több, akkor üres.

Ha még nincs erőforrás, akkor egy üres tömböt ad vissza végpont.

```json
{
  "data": {
    "genre": [
      {
        "id": 1,
        "name": "Rock&Roll"
      },
      {
        "id": 2,
        "name": "Jazz"
      },
      {
        "id": 3,
        "name": "Classical"
      }
    ]
  },
  "pagination": {
    "count": 12,
    "per_page": 3,
    "current_page": 1,
    "total_pages": 84,
    "next_url": "/genre?page=2&number=3"
  }
}
```

# Resource Létrehozása

## HTTP Kérés

### Végpont

POST `/genre`

### Body

```json
{
  "name": "Rock&Roll"
}
```

## Sikeres Válasz

Sikeres válasz esetén a végpont visszatér a létrehozott erőforrás
adataival, amely tartalmazza, az adott resource eléréséhez szükséges
azonosítót vagy slug-ot.

### HTTP Status Code

201

### Body

```json
{
  "data": {
    "id": 1,
    "name": "Rock&Roll"
  }
}
```

## Sikertelen válaszok

### Invalid argumentum

**HTTP CODE**: 400

```json
{
  "error": {
    "code": "ERR_MISSING_ARGUMENT",
    "title": "Missing Argument",
    "message": "The name field is required!"
  }
}
```

# Adott Resource Törlése

## HTTP Kérés

### Végpont

DELETE `/genre/${genre-id}` vagy DELETE `/genre/${genre-slug}`

A lényeg, hogy az adott erőforrást egyértelműen beazonosító jellemzővel
érhető el az adott erőforrás törlése, mint pl. az adott rekord azonosítója,
vagy egy SLUG `/genre/rock-and-roll`. A listázó válaszban szerepelnie kell
ezen jellemzőnek!

## Sikeres Válasz

Sikeres válasz esetén, az adott végpont visszatér a törölt erőforrás jellemzőivel.

```json
{
  "data": {
    "id": 1,
    "name": "Rock&Roll"
  }
}
```

## Sikertelen válaszok

### Az adott erőforrást már törölték

**HTTP CODE**: 410

```json
{
  "error": {
    "code": "ERR_RESOURCE_DELETED",
    "title": "Deleted Resource",
    "message": "The requested tag has been deleted"
  }
}
```

### Az adott erőforrás nem létezik

**HTTP CODE**: 404

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested tag does not exist"
  }
}
```


# Adott Resource Törlése

## HTTP Kérés

### Végpont

PUT `/genre/${genre-id}` vagy PUT `/genre/${genre-slug}`

A lényeg, hogy az adott erőforrást egyértelműen beazonosító jellemzővel
érhető el az adott erőforrás törlése, mint pl. az adott rekord azonosítója,
vagy egy SLUG `/genre/rock-and-roll`. A listázó válaszban szerepelnie kell
ezen jellemzőnek!

### Body

```json
{
  "name": "FeriMetal"
}
```

## Sikeres Válasz

Sikeres válasz esetén, az adott végpont visszatér a frissített erőforrás jellemzőivel.

```json
{
  "data": {
    "id": 1,
    "name": "FeriMetal"
  }
}
```

## Sikertelen válaszok

### Az adott erőforrást már törölték

**HTTP CODE**: 410

```json
{
  "error": {
    "code": "ERR_RESOURCE_DELETED",
    "title": "Deleted Resource",
    "message": "The requested tag has been deleted"
  }
}
```

### Az adott erőforrás nem létezik

**HTTP CODE**: 404

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested tag does not exist"
  }
}
```
