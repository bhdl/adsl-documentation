# Cloud Webapp Service

## Leírás

A Cloud Webapp egy olyan alkalmamzás, amely webes felületet nyújt törzsadatok adminisztrációja, valamint BI funkciók elérésére.

A továbbiakban a rendszer entitás diagramját és API interfészét részletezzük.

## Cloud Webapp Entitás Diagram

![Cloud Webapp Entity Diagram](./img/webapp-entity-chart.bmp)

# Cloud Webapp Leltározás REST API
A REST API kialakítása a [general dokumentum](/comm_general_rest) alapján.
## Partner API

### Adatok leírása
| Név                      | Típus              | Leírás                                  |
|--------------------------|--------------------|-----------------------------------------|
| data                     | Object             | Adatcsomag                              |
| data.partnerId           | Integer            | A partner egyedi azonosítója            |
| data.name                | String             | A partner megnevezése                   |

### GET Partner

#### Egy partner lekérdezése
<pre>GET /rest/v1/partner/1</pre>

Sikeres válasz: HTTP 200
```json
{
  "data": {
    "partnerId": 1,
    "name": "ADSL-Partner"
  }
}
```

Sikertelen válasz: HTTP 404
```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

#### Összes Partner lekérdezése
<pre>GET /rest/v1/partner</pre>
Sikeres válasz: HTTP 200
```json
{
  "data": {
    "partners": [
      {
        "partnerId": 1,
        "name": "ADSL-Partner"
      },
      {
        "partnerId": 2,
        "name": "ADSL-Partner1"
      },
      {
        "partnerId": 3,
        "name": "ADSL-Partner2"
      }
    ],
    "pagination": {
      "count": 2,
      "per_page": 10,
      "current_page": 1,
      "total_pages": 1,
      "next_url": ""
    }
  }
}
```

### Partner létrehozása
<pre>POST /rest/v1/partner</pre>
```json
{
  "name": "Partner3"
}
```

Sikeres válasz: HTTP 201
```json
{
  "data": {
    "partnerId": 3,
    "name": "Partner3"
  }
}
```

Sikertelen válasz: HTTP 406

Létező partnernév
```json
{
  "error": {
    "code": "ERR_WRONG_VALUE",
    "title": "Wrong value",
    "message": "The name you entered is currently being used by another partner"
  }
}
```

Sikertelen válasz: HTTP 400

Kötelező mező
```json
{
  "error": {
    "code": "ERR_MISSING_ARGUMENT",
    "title": "Missing Argument",
    "message": "The name is required!"
  }
}
```

### Partner törlése
<pre>DELETE /rest/v1/partner/1</pre>

Sikeres válasz: HTTP 200
```json
{
  "data": {
    "partnerId": 1,
    "name": "Partner1"
  }
}
```

Sikertelen válasz: HTTP 404

A törölni kívánt elem nem létezik
```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

### Partner módosítása
<pre>PUT /rest/v1/partner/1</pre>

Sikeres válasz: HTTP 200
```json
{
  "data": {
    "partnerId": 2,
    "name": "Partner007"
  }
}
```

Sikertelen válasz: HTTP 404
```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

## Termék API

### Adatok leírása

| Név                         | Típus                | Leírás                                                   |
|-----------------------------|----------------------|----------------------------------------------------------|
| data                        | Object               | Adatcsomag                                               |
| data.productId              | Integer              | A termék egyedi azonosítója                              |
| data.partnerId              | Integer              | A partner egyedi azonosítója amelyhez tartozik a termék  |
| data.name                   | String               | Termék megnevezése, amely partnerenként egyedi           |
| data.description            | String               | Termék leírása                                           |


### GET Product

#### Egy termék lekérdezése

<pre>GET /rest/v1/product/1</pre>

Sikeres válasz: HTTP 200
```json
{
  "data": {
    "productId": 1,
    "partnerId": 1,
    "name": "Product1",
    "description": "Long description"
  }
}
```

Sikertelen válasz: HTTP 404
```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

#### Összes termék lekérdezése

<pre>GET /rest/v1/product</pre>

Sikeres válasz: HTTP 200
```json
{
  "data": {
    "products": [
      {
        "productId": 1,
        "partnerId": 1,
        "name": "Product1",
        "description": "Long description"
      },
      {
        "productId": 2,
        "partnerId": 1,
        "name": "Product2",
        "description": "Another description"
      }
    ],
    "pagination": {
      "count": 2,
      "per_page": 10,
      "current_page": 1,
      "total_pages": 1,
      "next_url": ""
    }
  }
}
```

### Termék létrehozása

<pre>POST /rest/v1/product</pre>

```json
{
  "partnerId": 1,
  "name": "Product3",
  "description": "It's a description"
}
```

Sikeres válasz: HTTP 201
```json
{
  "data": {
    "productId": 3,
    "partnerId": 1,
    "name": "Product3",
    "description": "It's a description"
  }
}
```

Sikertelen válasz: HTTP 406

Adott partnerhez létező termék

```json
{
  "error": {
    "code": "ERR_WRONG_VALUE",
    "title": "Wrong value",
    "message": "The name you entered is currently being used by another product!"
  }
}
```

Sikertelen válasz: HTTP 400

Kötelező mező

```json
{
  "error": {
    "code": "ERR_MISSING_ARGUMENT",
    "title": "Missing Argument",
    "message": "The name is required!"
  }
}
```

### Termék törlése

<pre>DELETE /rest/v1/product/1</pre>

Sikeres válasz: HTTP 200
```json
{
  "data": {
    "productId": 3,
    "partnerId": 1,
    "name": "Product3",
    "description": "It's a description"
  }
}
```

Sikertelen válasz: HTTP 404

A törölni kívánt elem nem létezik

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

### Termék módosítása

<pre>PUT /rest/v1/product/1</pre>

Sikeres válasz: HTTP 200
```json
{
  "data": {
    "productId": 1,
    "partnerId": 1,
    "name": "Product11",
    "description": "It's a description"
  }
}
```

Sikertelen válasz: HTTP 404
```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

## Telephely API

### Adatok leírása

| Név                    | Típus     | Leírás                                                          |
|------------------------|-----------|-----------------------------------------------------------------|
| data                   | Object    | Adatcsomag                                                      |
| data.siteId            | Integer   | Telephely egyedi azonosítója                                    |
| data.partnerId         | Integer   | Partner egyedi azonosítója amelyhez tartozik az adott telephely |
| data.gatewayId         | String    | A gateway egyedi azonosítója                                    |
| data.address           | String    | A telephely címe                                                |
| data.projection        | String    | A térkép projektációja. Alapértelmezetten "mercator"            |
| data.topLatitude       | Float     | Az épület felső falának geolokációs koordinátája                |
| data.bottomLatitude    | Float     | Az épület alsó falának geolokációs koordinátája                 |
| data.rightLongitude    | Float     | Az épület jobb falának geolokációs koordinátája                 |
| data.leftLongitude     | Float     | Az épület bal falának geolokációs koordinátája                  |
| data.mapSvg            | String    | Az épület alaprajzához tartozó adatok JSON stringként tárolva        |

#### mapSvg

JSON tömb, stringként tárolva.

Az Ammap nevű térkép plugin számára az épület alaprajzához szükséges alakzatok megjelenítésére, melyek ebben a verzióban lehetnek line vagy polygon típusúak. A vonalak pontjait `longitude` és `latitude` értékekkel kell megadni.

A JSON validációja az alábbi JSON Schema-val valósul meg:

```json
{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "http://api.example.com/profile.json#",
    "type": "array",
    "uniqueItems": true,
    "items": {
        "type": "object",
        "properties": {
            "type": {
                "type": "string",
                "minLenght": 1,
                "maxLength": 64
            },
            "points": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "longitude": {
                            "type": "number"
                        },
                        "latitude": {
                            "type": "number"
                        }
                    },
                    "required": ["longitude", "latitude"],
                    "additionalProperties": false
                }
            }
        },
        "required": ["type", "points"],
        "additionalProperties": false
    }
}
```

**Minta adat**

```json
[
    {"type:" "line", "points": [{ "longitude": 123.222, "latitude": 123.122 }, {...}]},
    {"type:" "polygon", "points": [{ "longitude": 123.222, "latitude": 123.122 }, {...}]}
]
```

### GET Site

#### Egy telephely lekérdezése

<pre> GET /rest/v1/site/1 </pre>

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "siteId": 1,
    "partnerId": 1,
    "gatewayId": "gw-01",
    "address": "telephely 1",
    "projection": "mercator",
    "topLatitude": 47.693444,
    "bottomLatitude": 47.690512,
    "rightLongitude": 19.296237,
    "leftLongitude": 19.291946,
    "mapSvg": "[{\"type\": \"line\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.5, \"latitude\": 4.4}]},{\"type\": \"polygon\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.1, \"latitude\": 4.4},{\"longitude\": 3.5, \"latitude\": 4.4}]}]"
  }
}
```

Sikertelen válasz: HTTP 404

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```


#### Összes telephely lekérdezése

<pre> GET /rest/v1/site </pre>

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "sites": [
      {
        "siteId": 1,
        "partnerId": 1,
        "gatewayId": "gw-01",
        "address": "telephely 1",
        "projection": "mercator",
        "topLatitude": 47.693444,
        "bottomLatitude": 47.690512,
        "rightLongitude": 19.296237,
        "leftLongitude": 19.291946,
        "mapSvg": "[{\"type\": \"line\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.5, \"latitude\": 4.4}]},{\"type\": \"polygon\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.1, \"latitude\": 4.4},{\"longitude\": 3.5, \"latitude\": 4.4}]}]"
      },
      {
        "siteId": 2,
        "partnerId": 1,
        "gatewayId": "gw-02",
        "address": "telephely 2",
        "projection": "mercator",
        "topLatitude": 47.693444,
        "bottomLatitude": 47.690512,
        "rightLongitude": 19.296237,
        "leftLongitude": 19.291946,
        "mapSvg": "[{\"type\": \"line\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.5, \"latitude\": 4.4}]},{\"type\": \"polygon\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.1, \"latitude\": 4.4},{\"longitude\": 3.5, \"latitude\": 4.4}]}]"
      },
      ...
    ],
    "pagination": {
      "count": 3,
      "per_page": 10,
      "current_page": 1,
      "total_pages": 1,
      "next_url": ""
    }
  }
}
```

### Telephely létrehozása

<pre> POST /rest/v1/site </pre>

```json
{
  "partnerId": 1,
  "gatewayId": "gw-008",
  "address": "telephely 1",
  "projection": "mercator" ,
  "topLatitude": 47.693444,
  "bottomLatitude": 47.690512,
  "rightLongitude": 19.296237,
  "leftLongitude": 19.291946,
  "mapSvg": "[{\"type\": \"line\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.5, \"latitude\": 4.4}]},{\"type\": \"polygon\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.1, \"latitude\": 4.4},{\"longitude\": 3.5, \"latitude\": 4.4}]}]"
}
```

Sikeres válasz: HTTP 201

```json
 {
  "data": {
    "siteId": 5,
    "partnerId": 1,
    "gatewayId": "gw-008",
    "address": "telephely 1",
    "projection": "mercator",
    "topLatitude": 47.693444,
    "bottomLatitude": 47.690512,
    "rightLongitude": 19.296237,
    "leftLongitude": 19.291946,
    "mapSvg": "[{\"type\": \"line\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.5, \"latitude\": 4.4}]},{\"type\": \"polygon\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.1, \"latitude\": 4.4},{\"longitude\": 3.5, \"latitude\": 4.4}]}]"
  }
}
```

Sikertelen válasz: HTTP 406

A gatewayId már tartozik valamilyen telephelyhez

```json
{
  "error": {
    "code": "ERR_WRONG_VALUE",
    "title": "Wrong value",
    "message": "Ez a Gateway ID már tartozik telephelyhez!"
  }
}
```

Sikertelen válasz: HTTP 400

Mező kitöltése kötelező

```json
{
  "error": {
    "code": "ERR_MISSING_ARGUMENT",
    "title": "Missing Argument",
    "message": "The gateway id is required!"
  }
}
```

### Telephely törlése

<pre> DELETE /rest/v1/site/3 </pre>

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "siteId": 3,
    "partnerId": 1,
    "gatewayId": "srth-1",
    "address": "Test Site Address",
    "projection": "mercator",
    "topLatitude": 47.693444,
    "bottomLatitude": 47.690512,
    "rightLongitude": 19.296237,
    "leftLongitude": 19.291946,
    "mapSvg": "[{\"type\": \"line\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.5, \"latitude\": 4.4}]},{\"type\": \"polygon\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.1, \"latitude\": 4.4},{\"longitude\": 3.5, \"latitude\": 4.4}]}]"
  }
}
```

Sikertelen válasz: HTTP 404

A törölni kívánt elem nem létezik.

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

### Telephely módosítása

<pre> PUT /rest/v1/site/1 </pre>

```json
{
  "partnerId": 1,
  "gatewayId": "new-gw-001",
  "address": "A telephely új címe",
  "projection": "mercator" ,
  "topLatitude": 47.693444,
  "bottomLatitude": 47.690512,
  "rightLongitude": 19.296237,
  "leftLongitude": 19.291946,
  "mapSvg": "[{\"type\": \"line\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.5, \"latitude\": 4.4}]},{\"type\": \"polygon\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.1, \"latitude\": 4.4},{\"longitude\": 3.5, \"latitude\": 4.4}]}]"
}
```

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "siteId": 1,
    "partnerId": 1,
    "gatewayId": "new-gw-001",
    "address": "A telephely új címe",
    "projection": "mercator",
    "topLatitude": 47.693444,
    "bottomLatitude": 47.690512,
    "rightLongitude": 19.296237,
    "leftLongitude": 19.291946,
    "mapSvg": "[{\"type\": \"line\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.5, \"latitude\": 4.4}]},{\"type\": \"polygon\", \"points\":[{\"longitude\": 3.5, \"latitude\": 4.4}, {\"longitude\": 3.1, \"latitude\": 4.4},{\"longitude\": 3.5, \"latitude\": 4.4}]}]"
  }
}
```

Sikertelen válasz: HTTP 406

A gatewayId már létezik telephelyhez

```json
{
  "error": {
    "code": "ERR_WRONG_VALUE",
    "title": "Wrong value",
    "message": "Ez a Gateway ID már tartozik telephelyhez!"
  }
}
```

Sikertelen válasz: HTTP 404

A módosítani kívánt elem nem létezik

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

## Tag API

### Adatok leírása

| Név                   | Típus                 | Leírás                            |
|-----------------------|-----------------------|-----------------------------------|
| data                  | Object                | Adatcsomag                        |
| data.tagId            | Integer               | Tag egyedi azonosítója            |
| data.siteId           | Integer               | Telephely egyedi azonosítója      |
| data.tagToken         | String                | Telephelyenként egyedi azonosító  |
| data.clientId         | String                | Pozicionáló jeladó azonosító (IPS Beacon)     |
| data.type             | String                | Tag típusa                        |
| data.name             | String                | Tag megnevezése                   |
| data.description      | String                | Tag leírása                       |

### GET Tag

#### Egy tag lekérdezése

<pre> GET /rest/v1/tag/1 </pre>

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "tagId": 1,
    "siteId": 1,
    "tagToken": "tagToken1",
    "clientId": "exClient1",
    "type": "forklift",
    "name": "First",
    "description": "Indoor"
  }
}
```

Sikertelen válasz: HTTP 404

A lekérdezni kívánt elem nem létezik

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

#### Összes tag lekérdezése

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "tags": [
      {
        "tagId": 1,
        "siteId": 1,
        "tagToken": "tagToken1",
        "clientId": "exClient1",
        "type": "forklift",
        "name": "First",
        "description": "Indoor"
      },
      {
        "tagId": 2,
        "siteId": 1,
        "tagToken": "tagToken2",
        "clientId": "exClient2",
        "type": "person",
        "name": "Joe",
        "description": "Indoor"
      },
      ...
    ],
    "pagination": {
      "count": 3,
      "per_page": 10,
      "current_page": 1,
      "total_pages": 1,
      "next_url": ""
    }
  }
}
```

### Tag létrehozása

<pre> POST /rest/v1/tag </pre>

```json
{
  "siteId": 1,
  "tagToken": "tt-002",
  "clientId": "ex-cid-001",
  "type":"forklift",
  "name":"Test Element",
  "description":"Outdoor forklift"
}
```

Sikeres válasz: HTTP 201

```json
{
  "data": {
    "tagId": 5,
    "siteId": 1,
    "tagToken": "tt-002",
    "clientId": "ex-cid-001",
    "type": "forklift",
    "name": "Test Element",
    "description": "Outdoor forklift"
  }
}
```

Sikertelen válasz: HTTP 406

```json
{
  "error": {
    "code": "ERR_WRONG_VALUE",
    "title": "Wrong value",
    "message": "TagToken is currently being used by another site"
  }
}
```

Sikertelen válasz: HTTP 400

```json
{
  "error": {
    "code": "ERR_MISSING_ARGUMENT",
    "title": "Missing Argument",
    "message": "The tagToken is required!"
  }
}
```

### Tag törlése

<pre> DELETE /rest/v1/tag/3 </pre>

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "tagId": 3,
    "siteId": 1,
    "tagToken": "tagToken3",
    "clientId": null,
    "type": "drone",
    "name": "Test",
    "description": "Outdoor"
  }
}
```

Sikertelen válasz: HTTP 404

A törölni kívánt elem nem létezik

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

### Tag módosítása

<pre> PUT /rest/v1/tag/1 </pre>

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "tagId": 1,
    "siteId": 1,
    "tagToken": "tt-102",
    "clientId": "ex-cid-101",
    "type": "forklift",
    "name": "Test Element",
    "description": "Outdoor forklift"
  }
}
```

Sikertelen válasz: HTTP 404

A módosítani kívánt elem nem létezik

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

## Leltározási esemény API

### Adatok leírása

| Név       | Típus   | Leírás                                       |
|-----------|---------|----------------------------------------------|
| siteId    | String  | **Kötelező**. A telephely egyedi azonosítója. |
| startTime | String  | **Kötelező**. A leltározási esemény kezdete.  |
| endTime   | String  | **Kötelező**. A leltározási esemény vége.     |


### GET inventoryEvent

#### Egy leltározási esemény lekérdezése

<pre> GET /rest/v1/inventoryEvent/1 </pre>

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "inventoryEventId": 1,
    "siteId": 1,
    "startTime": "2018-09-06 10:24:00",
    "endTime": "2018-09-07 10:24:00"
  }
}
```

Sikertelen válasz: HTTP 404

A lekérdezni kívánt elem nem létezik

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

#### Leltározási események listázása

<pre> GET /rest/v1/inventoryEvent </pre>

#### Query paraméterek
*   **page** (Integer): Hanyadik oldal
*   **number** (Integer): Mennyi esemény legyen a válaszban

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "inventoryEvents": [
      {
        "inventoryEventId": 1,
        "siteId": 1,
        "startTime": "2018-09-06 10:24:00",
        "endTime": "2018-09-07 10:24:00"
      }
    ],
    "pagination": {
      "count": 2,
      "per_page": 1,
      "current_page": 1,
      "total_pages": 2,
      "next_url": "/inventoryEvent/?page=2&number=1"
    }
  }
}
```

### POST inventoryEvent

#### Leltározási esemény létrehozása

<pre> POST /rest/v1/inventoryEvent </pre>

```json
{
  "siteId": 1,
  "startTime": "2018-09-06 18:00:00",
  "endTime": "2018-09-07 20:00:00"
}
```

Sikeres válasz: HTTP 201

```json
{
  "data": [
    {
      "inventoryEventId": 3,
      "siteId": 1,
      "startTime": "2018-09-06 18:00:00",
      "endTime": "2018-09-06 18:00:00"
    }
  ]
}
```

Sikertelen válasz: HTTP 400

```json
{
  "error": {
    "code": "ERR_MISSING_ARGUMENT",
    "title": "Missing Argument",
    "message": "The site is required!"
  }
}
```

#### Leltározási esemény törlése

<pre> DELETE /rest/v1/inventoryEvent/3 </pre>

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "inventoryEventId": 3,
    "siteId": 1,
    "startTime": "2018-09-06 18:00:00",
    "endTime": "2018-09-06 18:00:00"
  }
}
```

Sikertelen válasz: HTTP 404

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

#### Leltározási esemény módosítása

<pre> PUT /rest/v1/inventoryEvent/1 </pre>

```json
{
  "siteId": 2,
  "startTime": "2018-09-29 09:00:00",
  "endTime": "2018-09-29 20:00:00"
}
```

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "inventoryEventId": 1,
    "siteId": 2,
    "startTime": 1536222240,
    "endTime": 1536308640,
    "gatewayId": "gw-02",
    "tag": []
  }
}
```

Sikertelen válasz: HTTP 404

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist"
  }
}
```

## Leltározási státusz API

### Adatok leírása

| Név                           | Típus   | Leírás                                                |
|-------------------------------|---------|-------------------------------------------------------|
| inventoryEventId              | Integer | A leltározási esemény egyedi azonosítója              |
| inventoryEvents               | List    | Mért pozíciós adatok listája                          |
| inventoryEvents[i]            | Object  | Adott méréshez tartozó adatok                         |
| inventoryEvents[i].rfid       | String  | RFID                                                  |
| inventoryEvents[i].x          | Float   | Mért pozíció relatív x koordinátája méterben          |
| inventoryEvents[i].y          | Float   | Mért pozíció relatív y koordinátája méterben          |
| inventoryEvents[i].z          | Float   | Mért pozíció relatív z koordinátája méterben          |
| inventoryEvents[i].accuracy   | Float   | Mért pozíció pontossága méterben                      |
| inventoryEvents[i].latitude   | Float   | Mért pozíció geolokációban mért pozíciója - szélesség |
| inventoryEvents[i].longitude  | Float   | Mért pozíció geolokációban mért pozíciója - hosszúság |


### GET inventoryStatus

#### Egy leltározási esemény lekérdezése

<pre> GET /rest/v1/inventoryStatus/1 </pre>

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "inventoryStatusId": 2,
    "inventoryEventId": 1,
    "rfid": "randomrfid002",
    "x": 1.5,
    "y": 1.5,
    "z": 1.5,
    "accuracy": 0.8,
    "latitude": 30.63,
    "longitude": 67.115
  }
}
```

Sikertelen válasz: HTTP 404

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```

#### Leltározási státuszok listázása

<pre> GET /rest/v1/inventoryStatus </pre>

#### Query paraméterek
* **page** (Integer): Hanyadik oldal
* **number** (Integer): Mennyi státusz jelenjen meg a válaszban

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "inventoryStatuses": [
      {
        "inventoryStatusId": 1,
        "inventoryEventId": 1,
        "rfid": "randomrfid001",
        "x": 1.5,
        "y": 1.5,
        "z": 1.5,
        "accuracy": 0.8,
        "latitude": 30.63,
        "longitude": 67.115
      },
      {
        "inventoryStatusId": 2,
        "inventoryEventId": 1,
        "rfid": "randomrfid002",
        "x": 1.5,
        "y": 1.5,
        "z": 1.5,
        "accuracy": 0.8,
        "latitude": 30.63,
        "longitude": 67.115
      },
      ...
    ],
    "pagination": {
      "count": 8,
      "per_page": 10,
      "current_page": 1,
      "total_pages": 1,
      "next_url": ""
    }
  }
}
```

### POST inventoryStatus

#### Leltározási státusz létrehozása

<pre> POST /rest/v1/inventoryStatus </pre>

```json
{
  "inventoryEventId": 2,
  "inventoryEvents": [
    {
      "rfid": "exampleRFID1",
      "x": 1.5,
      "y": 1.5,
      "z": 1.5,
      "accuracy": 0.7,
      "latitude": 30.0007201,
      "longitude": 67.075301
    }, {
      "rfid": "exampleRFID1",
      "x": 1.5,
      "y": 1.5,
      "z": 1.5,
      "accuracy": 0.7,
      "latitude": 30.0007201,
      "longitude": 67.075301
    },
    ...
  ]
}
```

Sikeres válasz: HTTP 201

```json
{
  "data": [
    {
      "inventoryStatusId": 6,
      "inventoryEventId": 2,
      "rfid": "exampleRFID1",
      "x": 1.5,
      "y": 1.5,
      "z": 1.5,
      "accuracy": 0.7,
      "latitude": 30.0007,
      "longitude": 67.0753
    },
    {
      "inventoryStatusId": 7,
      "inventoryEventId": 2,
      "rfid": "exampleRFID1",
      "x": 1.5,
      "y": 1.5,
      "z": 1.5,
      "accuracy": 0.7,
      "latitude": 30.0007,
      "longitude": 67.0753
    },
    ...
  ]
}
```

Sikertelen válasz: HTTP 400

```json
{
  "error": {
    "code": "ERR_MISSING_ARGUMENT",
    "title": "Missing Argument",
    "message": "Inventory event ID is required!"
  }
}
```

### DELETE inventoryStatus

#### Leltározási státusz törlés

<pre> DELETE /rest/v1/inventoryStatus/3 </pre>

Sikeres válasz: HTTP 200

```json
{
  "data": {
    "inventoryStatusId": 3,
    "inventoryEventId": 2,
    "rfid": "exampleRFID1",
    "x": 1.5,
    "y": 1.5,
    "z": 1.5,
    "accuracy": 0.7,
    "latitude": 30.0007,
    "longitude": 67.0753
  }
}
```

Sikertelen válasz: HTTP 404

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested element does not exist!"
  }
}
```
