# Real Time Location System - RTLS

Beszállítói rendszer. A rendszer azt az adatot szolgáltatja REST API-n keresztül,
hogy a telephelyen lévő **tag**-ek milyen távolságra vannak a fixen telepített **anchor**-októl.
Ez alapján kiszámíthatóak az egyes **tag**-ek relatív koordinátái.

## Rendszereszközök térképe

![RTLS System Overview](img/rtls_overview.bmp)

## Rendszereszközök

### Central Unit

A **Central Unit** lokális hálózaton keresztül nyújt REST API felületet
a **tag**-ek **anchor**-okhoz képest mért távolsági adatok lekérdezéséhez.

### Anchor

Az **anchor**-ok bus-on keresztül csatlakotnak a **Central Unit**-hoz. Az
**anchor**-ok fixen elhelyezett referencia pontok a telephelyen belül.

### Tag

A **tag**-ek a megfigyelt objektumoknál (drónok, munkatársak, targoncák stb.)
lévő markerek. A **tag**-ek pozícióját határozhatjuk meg a rendszer által az
**anchor**-okhoz képest nyújtott távolsági adatok alapján.

A jeladók azaz **tag**-ek kibocsátanak egy ID-t és egy timestampet, amit a detektorok
azaz **anchor**-ok érzékelnek. Az idő különbségből pedig a rendszer számol egy távolságot.


# RTLS REST API

Az RTLS rendszerét egy külső beszállító biztosítja. Az RTLS rendszerrel való együttműködést
REST API felület biztosítja.

## GET /tags

### API hívás leírása

A jeladók listáját adja vissza.

FONTOS: A legutóbb látott adatokat tartalmazza.
Ha egy jeladó nem sugároz többé, mindig a legutolsó ismert adat lesz benne.

### Példa válasz

```json
{
  "data": {
    "16": {
      "1": 2.22,
      "2": 4.01,
      "3": 2.3,
      "4": 3.08,
      "5": 4.08
    },
    "32": {
      "1": 2.13,
      "2": 4.23,
      "3": 2.5,
      "4": 3,
      "5": 4.59
    },
    "48": {
      "1": 2.26,
      "2": 4.03,
      "3": 2.58,
      "4": 3.26,
      "5": 4.72
    }
  }
}
```

### Példa válasz értelmezése

A `"16", "32", "48"` a **tag**-ek azonosítója. Az `"1", "2", "3", "4", "5"` az **anchor**-ok
azonosítója. Minden  **anchor** magadja az adott **tag** távolságát. Egyszerre
legfeljebb 6 **anchor** fog mindig frissülni.

## GET /changes

Ez az API végpont a legutóbbi API hívás utáni változásokat adja vissza.

Ha nem érzékelt semmit sem egy **anchor**, akkor üres JSON-t ad vissza.

Az API válasz formátuma megegyezik a **GET /tags** végpont válasz formátumával.

# Fejlesztői rendszer adatok

Az irodában működő demó rendszer adatai a Projekt paraméterek wiki oldalon találhatóak.

- Address: http://[RTLS_IP]
- Port: [RTLS_PORT]
