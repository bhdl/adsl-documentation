# Térkép megjelenítés áttekintése

A térképes frontend-es programkönyvtár megoldása a **Data Cloud**-tól
kapja meg a megjeleníteni kívánt marker pozíciós adatait.

A térkép frontend-es megoldása a Cloud Webapp-tól kapja meg
a lehetséges markerek alanyi változóit.

# Socket Kommunikáció Leírása

## Végpont

`/gateway/${gatewayId}/ips`

A végpont egyértelműen meghatározza, hogy a kliens mely telephelynek
az adatfolyamára iratkozik fel a *gateway azonosító* megadásával.

## Események

### on - mapdata

A real time gateway-től érkező IPS adatokat továbbítja átalakítva
a leírt adatszerkezet szerint.

A Webapp-ba integrált térképes frontend alkalmazás fogadja ezeket az adatokat és realtime megjeleníti, a szűrőfeltételek szerint.

**Példa**

```json
{
  "timestamp": 1534313277452,
  "clientId": "cloud01",
  "gatewayId": "gateway-01",
  "type": "map_data",
  "packageId": "cloud01_2018_08_21_10_43_21_123",
  "data": {
    "positions": [
      {
        "tagId": 100,
        "geolocation": {
          "latitude": 47.69312171673933,
          "longitude": 19.295479768310038,
          "accuracy": 0.85
        }
      }
    ]
  }
}
```

### Adatok leírása


| Név                                     | Típus   | Leírás                                                   |
|-----------------------------------------|---------|----------------------------------------------------------|
| timestamp                               | Long    | adatküldés időpontja UNIX-időben                         |
| clientId                                | String  | kliens cloud szinten egyedi azonosítója                  |
| gatewayId                               | String  | telephely azonosítója                                    |
| type                                    | String  | küldött információ osztályozása 'map_data'               |
| packageId                               | String  | küldött csomag azonosítója: client_id + "_" + timestamp  |
| data                                    | Object  | adatcsomag                                               |
| data.positions                          | List    | pozíciós adatok listája                                  |
| data.positions[i]                       | Object  | pozíciós adatok                                          |
| data.positions[i].tagId                 | Integer | követett objektum egyedi tag azonosítója                 |
| data.positions[i].timestamp             | Long    | időpontja UNIX-időben                                    |
| data.positions[i].geolocation           | Object  | követett objektum pozícionális adatai                    |
| data.positions[i].geolocation.latitude  | Number  | követett objektum geolokációban mért pozíciója szélesség |
| data.positions[i].geolocation.longitude | Number  | követett objektum geolokációban mért pozíciója hosszúság |
| data.positions[i].geolocation.accuracy  | Number  | küvetett objektum pozíció mérésének pontossága méterben  |


### on - error

Csak alapértelmezett error esemény van, ha hibás az érkező adatcsomag,
a gateway felől akkor a szolgáltatás nem továbbítja.


# Cloud Data Map Szimulátor

Tesztelési célból létre lett hozva egy map simulator, amivel tesztelhető a térképes megoldás.

[Map Simulator Repository](https://gitlab.com/bhdl/bhdl-map-simulator)

# Cloud Data Map REST API

## Cloud Data Map Leltározási esemény értesítés

A leltározási esemény végén a Webapp hívja az alábbi Cloud Data Map végpontot.

Ennek hatására a Map elkezdi az adatok összegyűjtését és feldolgozását.

POST Kérés: ```/v1/inventoryEvent```

```json
{
  "timestamp": 1534313277452,
  "type": "inventory_event",
  "data": {
    "inventoryEventId": 23,
    "startTime": 1534313277452,
    "endTime": 1534313277452,
    "siteId": 123
  }
}
```

Sikeres Válasz: HTTP 200

```json
{
  "timestamp": 1534313277452,
  "type": "inventory_event",
  "data": {
    "inventoryEventId": 23,
    "startTime": 1534313277452,
    "endTime": 1534313277452,
    "siteId": 123
  }
}
```

Hiba válasz: HTTP 500

```json
{
  "error": {
    "code": "ERR_RESOURCE_NOT_EXIST",
    "title": "Resource Not Exist",
    "message": "The requested tag does not exist"
  }
}
```
