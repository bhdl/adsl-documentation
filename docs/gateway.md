# Gateway Szerver Alkalmazás

A **Gateway App** egyik célja, hogy a telephelyen lévő rendszerkomponensekkel a kommunikációt
a lokális hálózaton keresztül lebonyolítsa, valamint a beérkező adatokat eltárolja addig amíg internet
kapcsoalton keresztül a **Cloud Service**-nek nem továbbítja. A **Gateway App** szolgáltatás biztosítja
a kommunikációt **web socket** protokollon keresztül az **On Site System** és a **Cloud System** között.

## Szerver elérhetőség
putty: `[GATEWAY_SSH_URL]`, `[GATEWAY_SSH_USER]`, `[GATEWAY_SSH_PASS]`

### RFID szolgáltatástól

`http://[GATEWAY_IP]:[GATEWAY_APP_PORT]/rfid`

### IPS Position szolgáltatástól

`http://[GATEWAY_IP]:[GATEWAY_APP_PORT]/ips`

A küldött csomagok leírása a kapcsolódó rendszereknél találhatók (RFID, IPS).

## Telepítés

Előfeltétel: Deploy key elérhető GitLab-ban.

Projekt klónozása pl. ebbe a mappába: `/home/arteries/adsl-gateway`

Project frissítés: `git pull`

Branch: `master`

a `docker/docker-compose.yaml` file linux rendszerre van előkészítve.

Szükség van egy `docker/data` mappára, amely tárolja a mongo adatbázist, collection-t.

`docker-compose up -d`

### Mongo Express elérhetősége

Basic Auth, ha be van állítva: `[GATEWAY_AUTH_USER]` / `]GATEWAY_AUTH_PASS]`

`http://[GATEWAY_IP]:[GATEWAY_MONGO_PORT]/`

### Belépés a mongo console-ba, auth nélkül

`docker exec -it mongo mongo`

### Belépés a mongo console-ba, authtal

`docker exec -it mongo mongo -u root -p [GATEWAY_MONGO_PASS]`

### Adatbázis létrehozása

`use adsl`

### Felhasználó létrehozása

`db.createUser({user: "[GATEWAY_MONGO_USER]", pwd: "[GATEWAY_MONGO_PASS]", roles: [{ role: "dbAdmin", db:"adsl"},{role: "readWrite", db:"adsl"}] } )`

### Collection létrehozása

`db.createCollection("adsl")`

### Alkalmazás előkészítése

- Előfeltétel, telepítve legyen a legújabb aktuális verziók:
    - node.js,
    - docker,
    - docker-compose,
    - az arteries user legyen a docker csoportban
    - pm2 globálisan
    - mc
- Csomagtelepítés a projekt könyvtárában: `yarn`
- Az alkalmazás .env paramétereit ki kell tölteni

### Alkalmazás menedzselése

- Futtatás: `pm2 "node index.js"`
- Újraindítás: `pm2 ls` Ebből melyik id-n fut az app, majd ezzel újraindítani: `pm2 restart <appid>`

## Gateway Funkciók

- folyamatosan továbbítja az üzeneteket a cloudnak, és bufferbe tárolja
- pozitív visszajelzés esetén törli a bufferből és mongoból a csomagokat
- a Bufferméret után a csomagokat a szerver helyi Mongo-ba menti
- ha csatlakozik a cloudhoz, akkor az összes tárolt üzenetet továbbítja a cloudnak

