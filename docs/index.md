# ADSL dokumentáció

Jelen dokumentum az ADSL (Arteries Drone System League) projektről ad egy áttekintő nézetet.

## Projekt áttekintése

![System Overview](img/adsl_system_overview.bmp)

## On Site System

Raktározási telephelyre kitelepített lokális rendszer

### Real Time Location System - RTLS

Beszállítói rendszer. A rendszer azt az adatot szolgáltatja REST API-n keresztül,
hogy a telephelyen lévő **tag**-ek milyen távolságra vannak a fixen telepített **anchor**-októl.
Ez alapján kiszámíthatóak az egyes **tag**-ek relatív koordinátái.

[RTLS dokumentáció](./rtls.md)

### Indoor Positioning System - IPS

Az **RTLS** REST API felületét pollingozva a lokális hálózaton jut hozzá
a **tag**-ek **anchor**-okhoz mért távolságához. A szolgáltatás kiszámítja a **tag**-ek
relatív 3 dimenziós koordinátáit, amelyet **TCP/IP socket** kapcsolaton keresztül
a lokális hálózaton továbbít a **Gateway** alkalmazásnak.

### RFID Service

Az **RFID Service** egy a drónon lévő **Raspberry PI** eszközön fut. Soros porton keresztül olvassa
ki az RFID információkat az RFID olvasó és 3 db RFID antenna hardver komponensekből. A szolgáltatás
adott időpontban kiolvasott RFID azonosítókat az észlelt jelerőséggel továbbítja a **Gateway**
alkalmazásnak **Websocket (SocketIO)** kapcsolaton keresztül a lokális hálózaton.

### Drone Controller

A **Drone Controller** egy android eszközön fut. A szolgáltatás a DJI által szállított **Android SDK**-ra
épül. A szolgáltatás célja, hogy adott drón pozíciójának ismeretében végignavigálja magát a drónt a bemeneti
paraméterként megadott útvonalon.

### Gateway App

A **Gateway App** egyik célja, hogy a telephelyen lévő rendszerkomponensekkel a kommunikációt
a lokális hálózaton keresztül lebonyolítsa, valamint a beérkező adatokat eltárolja addig amíg internet
kapcsoalton keresztül a **Cloud Service**-nek nem továbbítja. A **Gateway App** szolgáltatás biztosítja
a kommunikációt **web socket** protokollon keresztül az **On Site System** és a **Cloud System** között.

*TODO: Tisztázni, hogy szükséges-e valamilyen kontorll funckió lokálisan*

## Cloud Service

A **Cloud Service** egy komplex felhőben lévő szolgáltatás, amely:

- Webes felületet nyújt törzsadatok adminisztrációja
- **REST API** felületet szolgáltat 3. fél által szállított rendszerek integrációjához
- **BI felület**: Kimutatásokat, statisztikákat, jelenít meg
- Fenntartja a kommunikációs kapcsoaltot a lokálisan telepített **On Site System** rendszerekkel.

### Cloud Data Server

A **Cloud Data** szolgáltatás tartja fenn a kapcsolatot **websocket**-en keresztül
az **On Site System** - **Gateway App** szolgáltatásával. A szolgáltatás egyik feladata, hogy az
**On Site System** rendszeréből érkező adatokat fogadja és eltárolja.

### Cloud Webapp

A **Cloud Webapp** egy olyan alkalmamzás, amely webes felületet nyújt törzsadatok adminisztrációja,
valamint **BI** funkciók elérésére.

### Cloud Data Mapper

A **Cloud Data Mapper** kiolvassa a **Cloud Data** szolgáltatásban eltárolt nyersadatokat,
s azokat előfeldolgozva átemeli a Cloud Webapp alkalmazás perzisztens háttértárába.

### 3rd Party Services

Olyan 3. szereplő által szállított megoldások, amelyekkel a jövőben integrálni kell majd az elkészülő rendszert.
A **Cloud Service** API felületet kell, hogy biztosítson, a 3. szereplő által szállított megoldások illesztéséhez.

## Rendszer szereplői

### Rendszer adminisztrátor

Hozzáfér a webes adminisztrációs felülethez, fel tud venni minden entitást, hozzáfér minden funkcióhoz.

### Leltározó

Hozzáfér a webes adminisztrációs felülethez, elindítja a leltározást, olvasási joga van a leltározási eredményekhez. Fel tud venni termékekeket, bevételez, kiad.

### Raktári munkatárs

Mobil eszközzel rendelkező munkatárs, aki a raktárban mozog.

### Drón

A leltározást végző repülő eszköz.

