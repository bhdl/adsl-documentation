# ADSL README

This is the documentation repo for **Arteries Drone System League** (ADSL) project.

## Project Documentation

- URL: project_url
- username: `adsl`
- password: `password`

## Documentation Engine

[MkDocs](https://www.mkdocs.org/)

## Prerequisites

* MkDocs [Official Documentation](https://www.mkdocs.org/#installation)
* Node.js [Official Site](https://nodejs.org/en/)

## Setup Deploy

0. `npm install` - Install dependencies
1. `cp ./example.deploy.config.js ./deploy.config.js` - Copy the config file, and give the ftp username & password
2. `npm run serve` - Run the builtin development server

## Useful Commands

* `npm run serve` - Run the builtin development server
* `npm run build` - Build the MkDocs documentation
* `npm run deploy` - Deploy Documentation to the server
