const chalk = require('chalk');
const fs = require('fs-extra');
const FtpDeploy = require('ftp-deploy');

const config = require('./deploy.config');


const log = console.log;
const ftpDeploy = new FtpDeploy();

try {
  fs.copySync('./password/.htaccess', './site/.htaccess');
  fs.copySync('./password/.htpasswd', './site/.htpasswd');
  log(chalk.green('Passwed protection has been successfully added!'));
} catch (err) {
  log(chalk.red(err));
}


ftpDeploy.deploy(config)
  .then(res => log(chalk.green('Deploy finished')))
  .catch(err => log(chalk.red(err)));
