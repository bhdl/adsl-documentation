const config = {
  user: "user",
  password: "password",
  host: "ftp.host",
  port: 21,
  localRoot: __dirname + '/site',
  remoteRoot: '/adsl/public',
  include: ['*', '**/*', '.htaccess', '.htpasswd'],
  exclude: [],
  deleteRemote: false
}

module.exports = config;
